const fastify = require('fastify')
const fetch = require('node-fetch')

const config = require('./config')
const logger = require('./logger')
const cors = require('./cors')
const hello = require('./hello')

const app = fastify({
  logger,
})

app.decorate('config', config)
app.addHook('onRequest', cors(config.prefix))

app.decorate('fetch', fetch)
app.decorate('fetchJSON', fetchJSON)

// wild module appears:
app.register(hello.register)

module.exports = app

async function fetchJSON(url, opts) {
  const req = await fetch(url, opts)
  req.data = await req.json()
  return req
}

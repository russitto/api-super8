#!/usr/bin/env node

const app = require('./app')

app.listen(app.config.port, '0.0.0.0')

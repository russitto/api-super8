const greet = {
  tags: ['hello'],
  summary: 'Greet',
  description: 'Say Hi!',
  response: {
    200: {
      description: 'hi hello',
      type: 'object',
      properties: {
        hi: { type: 'string' },
      }
    },
  },
}

const ping = {
  tags: ['hello'],
  summary: 'Ping',
  description: 'Say Ping!',
  response: {
    200: {
      description: 'Pong',
      type: 'string',
    },
  },
}

module.exports = {
  greet,
  ping,
}

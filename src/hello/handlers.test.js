const Ajv = require('ajv')

const app = require('../app')
const handlers = require('./handlers')
const schemas = require('./schemas')

const ajv = new Ajv()

describe('** hello', () => {
  let fastify
  beforeAll(() => {
    fastify = app
  })
  afterAll(() => {
    fastify.close()
  })

  test('/: hi, hello', async () => {
    const validate = ajv.compile(schemas.greet.response[200])
    const response = await fastify.inject({
      method: 'GET',
      url: '/'
    })
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toMatch(/^application\/json.*/)
    expect(validate(JSON.parse(response.payload))).toBeTruthy()
  })

  test('ping: pong!', async () => {
    const validate = ajv.compile(schemas.ping.response[200])
    const response = await fastify.inject({
      method: 'GET',
      url: fastify.config.prefix + '/ping'
    })
    expect(response.statusCode).toBe(200)
    expect(response.headers['content-type']).toMatch(/^application\/json.*/)
    expect(validate(JSON.parse(response.payload))).toBeTruthy()
    expect(response.payload).toBe('"pong"')
  })
})

const handlers = require('./handlers')
const schemas = require('./schemas')

module.exports = {
  handlers,
  schemas,
  register,
}

function register(fastify, options, done) {
  fastify.get('/', schemas.greet, handlers.greet)
  fastify.get(fastify.config.prefix + '/greet', schemas.greet, handlers.greet)
  fastify.get(fastify.config.prefix + '/ping', schemas.ping, handlers.ping)
  done()
}

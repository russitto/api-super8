module.exports = {
  greet,
  ping,
}

function greet() {
  return {
    hi: 'hello',
  }
}

function ping(request, reply) {
  reply.type('application/json').send('"pong"')
}

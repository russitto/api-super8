const app = require('./app')

describe('** fetchJSON', () => {
  let fastify
  beforeAll(() => {
    fastify = app
  })
  afterAll(() => {
    fastify.close()
  })

  test('remote JSON', async () => {
    const req = await fastify.fetchJSON('https://jsonplaceholder.typicode.com/todos/1')
    expect(req).toBeDefined()
    expect(req.status).toBe(200)
    expect(req).toHaveProperty('body')
    expect(req).toHaveProperty('data')
    expect(req.data).toBeDefined()
  })

  test('remote HTML', async () => {
    let req
    try {
      req = await fastify.fetchJSON('https://www.google.com.ar/')
    } catch(e) {
      expect(e.type).toBe('invalid-json')
    }
  })
})

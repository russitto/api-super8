const app = require('./app')
const cors = require('./cors')

describe('** cors', () => {
  let fastify
  beforeAll(() => {
    fastify = app
  })
  afterAll(() => {
    fastify.close()
  })

  test('/', async () => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/'
    })
    expect(response.statusCode).toBe(200)
    expect(response.headers['access-control-allow-origin']).toBe('*')
    expect(response.headers['access-control-allow-methods']).toBe('GET, POST, DELETE, HEAD, OPTIONS')
    expect(response.headers['access-control-allow-headers']).toBe('Origin, X-Requested-With, Content-Type, Accept, Range, Authorization, X-Request-ID')
  })

  test('/404notfound', async () => {
    const response = await fastify.inject({
      method: 'GET',
      url: '/404notfound'
    })
    expect(response.statusCode).toBe(404)
    expect(response.headers['access-control-allow-origin']).toBe('*')
    expect(response.headers['access-control-allow-methods']).toBe('GET, POST, DELETE, HEAD, OPTIONS')
    expect(response.headers['access-control-allow-headers']).toBe('Origin, X-Requested-With, Content-Type, Accept, Range, Authorization, X-Request-ID')
  })

  test('/ HEAD does not matters', async () => {
    const response = await fastify.inject({
      method: 'HEAD',
      url: '/'
    })
    expect(response.headers).not.toHaveProperty('access-control-allow-origin')
    expect(response.headers).not.toHaveProperty('access-control-allow-methods')
    expect(response.headers).not.toHaveProperty('access-control-allow-headers')
  })

  test('/404notfoundoptions has 1 more header', async () => {
    const response = await fastify.inject({
      method: 'OPTIONS',
      url: '/404notfoundoptions'
    })
    // allways to be 200
    expect(response.statusCode).toBe(200)
    expect(response.headers['access-control-max-age']).toBe('86400')
    expect(response.headers['access-control-allow-origin']).toBe('*')
    expect(response.headers['access-control-allow-methods']).toBe('GET, POST, DELETE, HEAD, OPTIONS')
    expect(response.headers['access-control-allow-headers']).toBe('Origin, X-Requested-With, Content-Type, Accept, Range, Authorization, X-Request-ID')
  })
})

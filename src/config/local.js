const port = 9000
const version = 'v1'
const prefix = '/api/' + version

module.exports = {
  port,
  version,
  prefix,
}
